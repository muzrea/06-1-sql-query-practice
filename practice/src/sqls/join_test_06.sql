/*
 * 请告诉我所有的员工（employee）的姓名及其管理者的姓名。所有的姓名都需要按照 `lastName, firstName`
 * 的格式输出，例如 'Bow, Anthony'。如果员工没有管理者，则其管理者的姓名输出为 '(Top Manager)'。
 * 输出需要包含如下信息：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */

SELECT CONCAT(first.lastName, ', ', first.firstName) AS employee,
       CONCAT(second.lastName, ', ', second.firstName) AS manager
FROM employees first,
     employees second
WHERE second.employeeNumber = first.reportsTo
UNION
SELECT CONCAT(lastName, ', ', firstName) AS employee,
       '(Top Manager)' AS manager
FROM employees
WHERE reportsTo IS NULL
ORDER BY manager, employee;
