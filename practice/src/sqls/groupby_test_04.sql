/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 * 
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */
SELECT orders.orderNumber,SUM(quantityOrdered*priceEach) AS totalPrice FROM orders
INNER JOIN orderdetails o ON orders.orderNumber = o.orderNumber
GROUP BY orders.orderNumber
HAVING SUM(quantityOrdered*priceEach) > 60000
ORDER BY orders.orderNumber;