/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */

SELECT MIN(orderItemCount)           AS minOrderItemCount,
       MAX(orderItemCount)           AS maxOrderItemCount,
       ROUND(AVG(orderItemCount), 0) AS avgOrderItemCount
FROM (SELECT COUNT(o.orderNumber) AS orderItemCount
      FROM orders
               INNER JOIN orderdetails o ON orders.orderNumber = o.orderNumber
      GROUP BY o.orderNumber) AS itemCount;
